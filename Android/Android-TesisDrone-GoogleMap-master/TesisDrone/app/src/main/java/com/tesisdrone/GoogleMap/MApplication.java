package com.tesisdrone.GoogleMap;

import android.app.Application;
import android.content.Context;

import com.secneo.sdk.Helper;

public class MApplication extends Application {

    private DroneApplication fpvDroneApplication;
    @Override
    protected void attachBaseContext(Context paramContext) {
        super.attachBaseContext(paramContext);
        Helper.install(MApplication.this);
        if (fpvDroneApplication == null) {
            fpvDroneApplication = new DroneApplication();
            fpvDroneApplication.setContext(this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        fpvDroneApplication.onCreate();
    }

}
