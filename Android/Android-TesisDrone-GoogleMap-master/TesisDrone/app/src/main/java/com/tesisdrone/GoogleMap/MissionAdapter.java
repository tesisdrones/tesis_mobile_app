package com.tesisdrone.GoogleMap;


import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.ThemedSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import dji.sdk.flighthub.model.User;

public class MissionAdapter extends ArrayAdapter<Mission> {
    public MissionAdapter(Context context, ArrayList<Mission> missions) {
        super(context, 0, missions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Mission mission = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_routing_from_web, parent, false);
        }
        // Lookup view for data population
        //TextView missionId = (TextView) convertView.findViewById(R.id.tvName);
        ListView missionName = (ListView) convertView.findViewById(R.id.routesList);
        // Populate the data into the template view using the data object
        //tvName.setText(user.name);
        //tvHome.setText(user.hometown);
        // Return the completed view to render on screen
        return convertView;
    }
}