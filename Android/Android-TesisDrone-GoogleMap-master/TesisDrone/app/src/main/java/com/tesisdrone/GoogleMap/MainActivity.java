package com.tesisdrone.GoogleMap;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity {

    //ListView superListView;
    boolean horizontalDisplay;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_main);
            horizontalDisplay = true;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_main);
            horizontalDisplay = false;
            if(findViewById(R.id.btn_waypoint1)!=null){
                findViewById(R.id.btn_waypoint1).setOnClickListener(clickListener);
                findViewById(R.id.btn_recorded_route).setOnClickListener(clickListener);
            }
        } else
            Toast.makeText(this, "Not defined", Toast.LENGTH_SHORT).show();
    }

    private View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.btn_waypoint1:
                startActivity(MainActivity.this, Waypoint1Activity.class);
                break;
            case R.id.btn_recorded_route:
                startActivity(MainActivity.this, RoutingFromWebActivity.class);
                break;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // if(!horizontalDisplay && findViewById(R.id.btn_waypoint1)!= null){
            findViewById(R.id.btn_waypoint1).setOnClickListener(clickListener);
            findViewById(R.id.btn_recorded_route).setOnClickListener(clickListener);
        }
        //superListView = findViewById(R.id.listaRutitas);
    //}

    public static void startActivity(Context context, Class activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
    }

}
