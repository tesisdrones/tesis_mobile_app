package com.tesisdrone.GoogleMap;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RouteData {
    @SerializedName("waypointCoordinates")
    private List<Double> waypointCoordinates = null;
    @SerializedName("treeCoordinates")
    private List<Double> treeCoordinates = null;
    @SerializedName("height")
    private Double height;

    public RouteData(List<Double> waypointCoordinates, List<Double> treeCoordinates, double height){
        this.waypointCoordinates = waypointCoordinates;
        this.treeCoordinates = waypointCoordinates;
        this.height = height;
    }

    //WAYPOINT
    public List<Double> getWaypointCoordinatesList() { return waypointCoordinates; }
    public void setWaypointCoordinates(List<Double> waypointCoordinates) { this.waypointCoordinates = waypointCoordinates; }


    //TREE
    public List<Double> getTreeCoordinatesList() { return treeCoordinates; }
    public void setTreeCoordinates(List<Double> treeCoordinates) { this.treeCoordinates = treeCoordinates; }

    //HEIGHT

    public Double getHeight() { return height; }
    public void setName(Double height) { this.height = height; }
}