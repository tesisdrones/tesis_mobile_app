package com.tesisdrone.GoogleMap;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {

    //String API_URL = "https://run.mocky.io/v3/";
    //@GET("794a529a-69b4-4269-9a33-a2368b55c032")

    String API_URL = "https://dev-tesis-sis-backend-dev.herokuapp.com/";
    @GET("mobileMissions")
    Call<List<Mission>> getAllMissions();
}
