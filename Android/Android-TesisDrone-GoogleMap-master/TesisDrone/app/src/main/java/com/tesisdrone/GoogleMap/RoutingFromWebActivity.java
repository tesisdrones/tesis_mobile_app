package com.tesisdrone.GoogleMap;

import androidx.annotation.Nullable;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dji.common.camera.SystemState;
import dji.common.error.DJIError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.flightcontroller.GPSSignalLevel;
import dji.common.mission.waypoint.Waypoint;
import dji.common.mission.waypoint.WaypointAction;
import dji.common.mission.waypoint.WaypointActionType;
import dji.common.mission.waypoint.WaypointMission;
import dji.common.mission.waypoint.WaypointMissionDownloadEvent;
import dji.common.mission.waypoint.WaypointMissionExecutionEvent;
import dji.common.mission.waypoint.WaypointMissionFinishedAction;
import dji.common.mission.waypoint.WaypointMissionFlightPathMode;
import dji.common.mission.waypoint.WaypointMissionHeadingMode;
import dji.common.mission.waypoint.WaypointMissionUploadEvent;
import dji.common.model.LocationCoordinate2D;
import dji.common.product.Model;
import dji.common.useraccount.UserAccountState;
import dji.common.util.CommonCallbacks;
import dji.sdk.base.BaseProduct;
import dji.sdk.camera.Camera;
import dji.sdk.camera.VideoFeeder;
import dji.sdk.codec.DJICodecManager;
import dji.sdk.flightcontroller.FlightAssistant;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.mission.timeline.triggers.WaypointReachedTrigger;
import dji.sdk.mission.waypoint.WaypointMissionOperator;
import dji.sdk.mission.waypoint.WaypointMissionOperatorListener;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKManager;
import dji.sdk.useraccount.UserAccountManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static android.view.View.GONE;
// import dji.ux.widget.*;

public class RoutingFromWebActivity extends FragmentActivity implements View.OnClickListener, GoogleMap.OnMapClickListener, OnMapReadyCallback, TextureView.SurfaceTextureListener {

    //Using Modes
    private boolean safe_starting_mode = true;
    private boolean safe_returning_mode = true;
    private boolean relative_altitudes_mode = true;
    private boolean goHomeSelected = false;
    private boolean isGhostWaypointReached = false;
    private boolean horizontalDisplay = false;
    private boolean fpv_mode = false;
    private boolean auto_tracking_drone_mode = false;

    //Video streaming vars
    protected VideoFeeder.VideoDataListener mReceivedVideoDataListener = null;

    // Codec for video live view
    protected DJICodecManager mCodecManager = null;
    protected TextureView mVideoSurface = null;

    // Mission list vars
    ListView superListView;
    Mission selectedMission;
    List<Mission> allMissions;

    //CP
    protected static final String TAG = "WaypointFromTheWeb";

    private GoogleMap gMapWeb;
    private Button load, settings, fly, locate, stop;

    private boolean isAdd = false;
    private double droneLocationLat = -34.930801, droneLocationLng = -56.159492;
    private final Map<Integer, Marker> mMarkers = new ConcurrentHashMap<Integer, Marker>();
    private Marker droneMarker = null;

    private float altitude = 100.0f;
    private float mSpeed = 10.0f;
    private int waypointHeading = 0;
    private int gimbalVertical = 0;

    // TakeoffLocation
    private Marker takeOffLocationMarker = null;
    private boolean isTakeOffLocation = false;
    private boolean isTakeOffLocationChanged = false;
    private LatLng takeOffLocationCoordinates = null;


    private List<Waypoint> waypointList = new ArrayList<>();

    public static WaypointMission.Builder waypointMissionBuilder;
    private FlightController mFlightController;
    private WaypointMissionOperator instance;

    //Check!!!
    private WaypointMissionFinishedAction mFinishedAction = WaypointMissionFinishedAction.NO_ACTION;
    private WaypointMissionHeadingMode mHeadingMode = WaypointMissionHeadingMode.AUTO;
    private LocationCoordinate2D Default_InterestPoint = new LocationCoordinate2D(-34.826744, -55.365813);


    // Navigation data GPS signal/Quality, Mission progress
    int waypointTargetIndex = 0;
    int waypointReady = 0;
    int waypointAmount;
    int satelliteAmount;

    String waypointProgress = "Progreso: " + Integer.toString(waypointReady) +"/" + Integer.toString(waypointAmount);
    boolean missionComplete = false;
    boolean missionStoped = false;

    //Dynamic Markers
    Marker dynamicMarker = null;

    //Googlemap Offset Fix for WGS84 21H UP TO DATE: 04-2021
    float latOffset = 0.000005070330189482775f;
    float longOffset = 0.00007775936248322068f;
    // float latOffset = 0;
    // float longOffset = 0;


    @Override
    public void onResume(){
        super.onResume();
        initFlightController();

        //video streaming
        Log.e(TAG, "onResume");
        initPreviewer();
        onProductChange();

        if(mVideoSurface == null) {
            Log.e(TAG, "mVideoSurface is null");
        }

    }

    /// video streaming
    protected void onProductChange() {
        initPreviewer();
    }

    @Override
    protected void onPause(){
        uninitPreviewer();
        super.onPause();
    }

    //video streaming
    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        unregisterReceiver(mReceiver);
        removeListener();
        //video streaming
        uninitPreviewer();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routing_from_web);
        if(!horizontalDisplay && findViewById(R.id.routesList)!= null){
            superListView = findViewById(R.id.routesList);
            getAllMissions();
        }


        // CP
        // When the compile and target version is higher than 22, please request the
        // following permissions at runtime to ensure the
        // SDK work well.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.VIBRATE,
                            Manifest.permission.INTERNET, Manifest.permission.ACCESS_WIFI_STATE,
                            Manifest.permission.WAKE_LOCK, Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CHANGE_WIFI_STATE, Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SYSTEM_ALERT_WINDOW,
                            Manifest.permission.READ_PHONE_STATE,
                    }
                    , 1);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(DroneApplication.FLAG_CONNECTION_CHANGE);
        registerReceiver(mReceiver, filter);



        initUI();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.webMissionMap);
        mapFragment.getMapAsync(this);

        addListener();
        if(!horizontalDisplay && findViewById(R.id.waypointTargetNumber)!= null){
            //waypoint current waypoint
            TextView waypointProgressNumber = (TextView)findViewById(R.id.waypointTargetNumber);
            //waypointNumber.setText(Integer.toString(this.waypointTargetIndex));
            waypointProgressNumber.setText("No hay mision seleccionada");
        }


        // END CP

        // for video streaming
        // The callback for receiving the raw H264 video data for camera live view
        mReceivedVideoDataListener = new VideoFeeder.VideoDataListener() {

            @Override
            public void onReceive(byte[] videoBuffer, int size) {
                if (mCodecManager != null) {
                    mCodecManager.sendDataToDecoder(videoBuffer, size);
                }
            }
        };

        Camera camera = DroneApplication.getCameraInstance();

        if (camera != null) {

            camera.setSystemStateCallback(new SystemState.Callback() {
                @Override
                public void onUpdate(SystemState cameraSystemState) {
                    if (null != cameraSystemState) {

                        int recordTime = cameraSystemState.getCurrentVideoRecordingTimeInSeconds();
                        int minutes = (recordTime % 3600) / 60;
                        int seconds = recordTime % 60;

                    }
                }
            });

        }
    }

    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            onProductConnectionChange();
        }
    };


    private void onProductConnectionChange()
    {
        initFlightController();
        loginAccount();
        initPreviewer();
    }

    private void loginAccount(){

        UserAccountManager.getInstance().logIntoDJIUserAccount(this,
                new CommonCallbacks.CompletionCallbackWith<UserAccountState>() {
                    @Override
                    public void onSuccess(final UserAccountState userAccountState) {
                        Log.e(TAG, "Login Exitoso");
                    }
                    @Override
                    public void onFailure(DJIError error) {
                        setResultToToast("Error de Login:"
                                + error.getDescription());
                    }
                });
    }

    private void initFlightController() {
        BaseProduct product = DroneApplication.getProductInstance();
        if (product != null && product.isConnected()) {
            if (product instanceof Aircraft) {
                mFlightController = ((Aircraft) product).getFlightController();
                FlightAssistant mFlightAssistant = mFlightController.getFlightAssistant();
                mFlightAssistant.setActiveObstacleAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("ActiveObstacleAvoidanceEnabled Activada!");
                        } else {
                            // setResultToToast("ActiveObstacleAvoidanceEnabled, error: " + djiError.getDescription());
                        }
                    }
                });

                //SAFE AND HIGHT PRECISION MODE SETTINGS

                mFlightAssistant.setAdvancedPilotAssistanceSystemEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("AdvancedPilotAssistanceSystemEnabled Activada!");
                        } else {
                            // setResultToToast("AdvancedPilotAssistanceSystemEnabled, error: " + djiError.getDescription());
                        }
                    }
                });

                mFlightAssistant.setCollisionAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("CollisionAvoidanceEnabled Activada!");
                        } else {
                            // setResultToToast("CollisionAvoidanceEnabled, error: " + djiError.getDescription());
                        }
                    }
                });

                mFlightAssistant.setVisionAssistedPositioningEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("VisionAssistedPositioningEnabled Activada!");
                        } else {
                            // setResultToToast("VisionAssistedPositioningEnabled, error: " + djiError.getDescription());
                        }
                    }
                });

                mFlightAssistant.setUpwardVisionObstacleAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("UpwardVisionObstacleAvoidanceEnabled Activada!");
                        } else {
                            // setResultToToast("UpwardVisionObstacleAvoidanceEnabled, error: " + djiError.getDescription());
                        }
                    }
                });
                mFlightAssistant.setHorizontalVisionObstacleAvoidanceEnabled(true,null);

                mFlightAssistant.setPrecisionLandingEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("PrecisionLandingEnabled Activada!");
                        } else {
                            // setResultToToast("PrecisionLandingEnabled, error: " + djiError.getDescription());
                        }
                    }
                });
                mFlightAssistant.setRTHObstacleAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("RTHObstacleAvoidanceEnabled Activada!");
                        } else {
                            // setResultToToast("RTHObstacleAvoidanceEnabled, error: " + djiError.getDescription());
                        }
                    }
                });

                mFlightAssistant.setRTHRemoteObstacleAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        if (djiError == null) {
                            // setResultToToast("RTHRemoteObstacleAvoidanceEnabled Activada!");
                        } else {
                            // setResultToToast("RTHRemoteObstacleAvoidanceEnabled, error: " + djiError.getDescription());
                        }
                    }
                });
                isTakeOffLocationChanged = true;
            }
        }

        if (mFlightController != null) {
            mFlightController.setStateCallback(new FlightControllerState.Callback() {

                @Override
                public void onUpdate(FlightControllerState djiFlightControllerCurrentState) {
                    droneLocationLat = djiFlightControllerCurrentState.getAircraftLocation().getLatitude();
                    droneLocationLng = djiFlightControllerCurrentState.getAircraftLocation().getLongitude();

                    // update takeOffLocation
                    if (mFlightController.getState().isHomeLocationSet()){
                        LocationCoordinate2D takeOffLocation2D =  mFlightController.getState().getHomeLocation();
                        takeOffLocationCoordinates = new LatLng(takeOffLocation2D.getLatitude()-latOffset, takeOffLocation2D.getLongitude()+longOffset);
                    }
                    updateDroneLocation();
                    updateSatelliteAmount();
                }
            });
        }
    }

    private void updateSatelliteAmount (){
        satelliteAmount = mFlightController.getState().getSatelliteCount();
        String satelliteAmountStatus = "Satélites Conectados: " + Integer.toString(satelliteAmount);
        if(!horizontalDisplay && findViewById(R.id.satelliteAmount)!= null) {

            TextView satelliteAmountview = (TextView) findViewById(R.id.satelliteAmount);
            satelliteAmountview.setText(satelliteAmountStatus);

            GPSSignalLevel gpsSignalQuality = mFlightController.getState().getGPSSignalLevel();
            String gpsSignalQualityStatus = "Calidad de Señal: " + Integer.toString(gpsSignalQuality.value());

            TextView gpsSignalQualityview = (TextView) findViewById(R.id.gpsSignalQuality);
            gpsSignalQualityview.setText(gpsSignalQualityStatus);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void updateDroneWaypointProgress (){
        //if(!horizontalDisplay && findViewById(R.id.waypointTargetNumber) != null) {
            GPSSignalLevel gpsSignalQuality = mFlightController.getState().getGPSSignalLevel();
            String gpsSignalQualityStatus = "Calidad de Señal: " + Integer.toString(gpsSignalQuality.value());
            TextView waypointProgressNumber = (TextView) findViewById(R.id.waypointTargetNumber);
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
            waypointProgress = "Progreso: " + Integer.toString(waypointReady) + "/" + Integer.toString(waypointAmount);


            if (this.waypointTargetIndex == this.waypointAmount && waypointTargetIndex != 0) {
                waypointProgress += " Estado: Finalizada";
            }
            waypointProgressNumber.setText(waypointProgress);
            progressBar.setMax(waypointAmount);
            progressBar.setProgress(Integer.valueOf(waypointReady), true);
        //}
    }

    // Update the drone location based on states from MCU.
    private void updateDroneLocation(){

        //Adding googleMaps offset to properly place the drone when taking photos in the app map
        droneLocationLng = droneLocationLng + longOffset;
        droneLocationLat = droneLocationLat - latOffset;
        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);

        //LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        final MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(pos);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.aircraft));

        //if (missionComplete || missionStoped) {
        //    showConfigUI();
        //}

        runOnUiThread(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void run() {
                if (droneMarker != null) {
                    droneMarker.remove();
                }

                if (checkGpsCoordination(droneLocationLat, droneLocationLng)) {
                    droneMarker = gMapWeb.addMarker(markerOptions);

                    //auto tracking
                    if ( auto_tracking_drone_mode ) {
                        cameraUpdate();
                    }
                }

                if (isTakeOffLocationChanged){
                    MarkerOptions takeOffMarkerOption = new MarkerOptions();
                    takeOffMarkerOption.position(takeOffLocationCoordinates);
                    takeOffMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.heliport));
                    takeOffLocationMarker = gMapWeb.addMarker(takeOffMarkerOption);
                    isTakeOffLocationChanged = false;
                }

                if(!missionStoped){

                    updateDroneWaypointProgress();

                   // int readyWaypointIndex = waypointTargetIndex -1;

                    int firstActualWaypoint;
                    int aimMarkerProxy;
                    if(safe_starting_mode){
                        firstActualWaypoint = 1;
                        aimMarkerProxy=1;
                    } else {
                        firstActualWaypoint =0;
                        aimMarkerProxy=0;
                    }

                    //Coloring Next Tree
                    if ( mMarkers.size()!=0 && waypointTargetIndex <= waypointAmount && waypointAmount !=0 && waypointTargetIndex>firstActualWaypoint) {
                        mMarkers.get(waypointTargetIndex-aimMarkerProxy).setIcon((bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_next)));

                    }

                    //Coloring Ready Tree
                    if(mMarkers.size()!=0 && waypointTargetIndex <= waypointAmount && waypointAmount !=0 && waypointTargetIndex > firstActualWaypoint){
                        mMarkers.get(waypointTargetIndex-(aimMarkerProxy+1)).setIcon((bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_ready)));
                    }

                    //Coloring last tree as ready
                    if(missionComplete){
                        //mMarkers.get(waypointAmount-1).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        waypointReady = waypointAmount;
                        mMarkers.get(waypointAmount-1).setIcon((bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_ready)));

                    }
                }
            }
        });
    }

    public static boolean checkGpsCoordination(double latitude, double longitude) {
        return (latitude > -90 && latitude < 90 && longitude > -180 && longitude < 180) && (latitude != 0f && longitude != 0f);
    }

    private void getAllMissions() {
        // MUST BE !horizontalDisplay
        if(!horizontalDisplay) {
            Call<List<Mission>> call = RetrofitBuilder.getInstance().getMyApi().getAllMissions();
            call.enqueue(new Callback<List<Mission>>() {
                @Override
                public void onResponse(Call<List<Mission>> call, Response<List<Mission>> response) {
                    List<Mission> missionList = response.body();
                    allMissions = missionList;
                    String[] aMission = new String[missionList.size()];

                    for (int i = 0; i < missionList.size(); i++) {
                        aMission[i] = missionList.get(i).getName();
                    }
                    superListView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, aMission));
                    superListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            selectMission(position);
                        }
                    });
                }

                @Override
                public void onFailure(Call<List<Mission>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "An error has occured", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void selectMission( int position){
        if(!horizontalDisplay) {
            mMarkers.clear();
            this.waypointTargetIndex = 0;
            this.waypointProgress = "Progreso: " + Integer.toString(waypointTargetIndex) + "/" + Integer.toString(waypointAmount);
            selectedMission = allMissions.get(position);
            Toast.makeText(getApplicationContext(), "Elegiste: " + selectedMission.getName(), Toast.LENGTH_LONG).show();
            loadMissionWaypointsOnMap();
            isGhostWaypointReached = false;
            //takeofflocation
            isTakeOffLocationChanged = true;
            mMarkers.get(waypointAmount - 1).setIcon((bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_on_list)));
        }
    }

    public void loadMissionWaypointsOnMap(){
        if(!horizontalDisplay) {

            Double[][] missionWaypointCoordinateSet = selectedMission.getWaypointCoordinatesArray();
            //an extra place for the ghost point
            LatLng[] missionPoints = new LatLng[missionWaypointCoordinateSet.length];

            Double[][] missionTreeCoordinateSet = selectedMission.getTreeCoordinatesArray();
            LatLng[] missionTrees = new LatLng[missionTreeCoordinateSet.length];

            Double[] missionHeightSet = selectedMission.getHeightArray();

            clearMissionMapWayPoints();
            clearWaypointMission();
            waypointList.clear();

            //Adding ghost point for safe starting mode at the first place
            LatLng aGhostPoint = new LatLng(missionWaypointCoordinateSet[0][0], missionWaypointCoordinateSet[0][1]);
            missionPoints[0] = aGhostPoint;
            int mGhostWaypointAltitude = 30;
            Waypoint mGhostWaypoint = new Waypoint(aGhostPoint.latitude, aGhostPoint.longitude, mGhostWaypointAltitude);
            //mGhostWaypoint.setHeadingInner(waypointHeading);

            if (waypointMissionBuilder != null) {
                waypointList.add(mGhostWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            } else {
                waypointMissionBuilder = new WaypointMission.Builder();
                waypointList.add(mGhostWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            }

            //Adding Mission Waypoints and Trees on Map
            for (int i = 0; i < missionWaypointCoordinateSet.length; i++) {
                LatLng aPoint = new LatLng(missionWaypointCoordinateSet[i][0], missionWaypointCoordinateSet[i][1]);
                //let first position for the ghost point
                missionPoints[i] = aPoint;
                LatLng aTree = new LatLng(missionTreeCoordinateSet[i][0], missionTreeCoordinateSet[i][1]);
                missionTrees[i] = aTree;
            }

            for (int i = 0; i < missionPoints.length; i++) {
                LatLng point = missionPoints[i];
                LatLng aTree = missionTrees[i];
                isAdd = true;

                if (isAdd == true) {
                    markWaypoint(aTree);
                    Waypoint mWaypoint = new Waypoint(point.latitude, point.longitude, altitude);
                    mWaypoint.altitude = missionHeightSet[i].floatValue();

                    if (waypointMissionBuilder != null) {
                        waypointList.add(mWaypoint);
                        waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
                    } else {
                        waypointMissionBuilder = new WaypointMission.Builder();
                        waypointList.add(mWaypoint);
                        waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
                    }
                    waypointAmount = missionPoints.length;
                    //ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                    //progressBar.setProgress(waypointReady);
                    //progressBar.setMax(waypointAmount);
                    setResultToToast("Cantidad de objetivos en la ruta: " + missionPoints.length);

                } else {
                    setResultToToast("Ha ocurrido un error con uno de los objetivos");
                }
                isAdd = false;
            }
            mMarkers.get(0).setIcon((bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_next)));
        }
    }

    // CP

    public void updateCurrentWaypointIndex(int waypointTargetIndex){
        if(!horizontalDisplay) {
            this.waypointTargetIndex = waypointTargetIndex;
            if (this.waypointTargetIndex == this.waypointAmount) {
                this.waypointProgress += " Estado: Finalizada";
            }
        }
    }

    public void updateCurrentWaypointIndexByOne(){
        if(!horizontalDisplay) {
            this.waypointTargetIndex++;
        }
    }

    //Add Listener for WaypointMissionOperator
    private void addListener() {
        if (getWaypointMissionOperator() != null){
            getWaypointMissionOperator().addListener(eventNotificationListener);
        }
    }

    private void removeListener() {
        if (getWaypointMissionOperator() != null) {
            getWaypointMissionOperator().removeListener(eventNotificationListener);
        }
    }

    private WaypointMissionOperatorListener eventNotificationListener = new WaypointMissionOperatorListener() {
        @Override
        public void onDownloadUpdate(WaypointMissionDownloadEvent downloadEvent) {

        }

        @Override
        public void onUploadUpdate(WaypointMissionUploadEvent uploadEvent) {

        }

        @Override
        public void onExecutionUpdate(WaypointMissionExecutionEvent executionEvent) {
            int nextWaypoint = executionEvent.getProgress().targetWaypointIndex;
            if(nextWaypoint>0 && !missionComplete){
                if(safe_starting_mode){
                    waypointReady = nextWaypoint-1;
                } else {
                    waypointReady = nextWaypoint;
                }
            }
                waypointTargetIndex = nextWaypoint;
        }

        @Override
        public void onExecutionStart() {
            // hideConfigUI();
            missionComplete = false;
            missionStoped = false;
        }

        @Override
        public void onExecutionFinish(@Nullable final DJIError error) {
            setResultToToast("Ejecución finalizada: " + (error == null ? "Con éxito!" : error.getDescription()));
            if(!missionStoped){
                missionComplete = true;
            }
            // showConfigUI();
        }
    };

    public WaypointMissionOperator getWaypointMissionOperator() {
        if (instance == null) {
            if (DJISDKManager.getInstance().getMissionControl() != null){
                instance = DJISDKManager.getInstance().getMissionControl().getWaypointMissionOperator();
            }
        }
        return instance;
    }

    private void initUI() {

        /*
            findViewById(R.id.btn_waypoint1).setOnClickListener(clickListener);
            findViewById(R.id.btn_recorded_route).setOnClickListener(clickListener);
         */
        if(!horizontalDisplay && findViewById(R.id.loadMissionBtn) != null){
            load = (Button) findViewById(R.id.loadMissionBtn);
            settings = (Button) findViewById(R.id.flySettingsBtn);
            fly = (Button) findViewById(R.id.flyBtn);
            locate = (Button) findViewById(R.id.locateBtn);
            stop = (Button) findViewById(R.id.stopBtn);

            load.setOnClickListener((View.OnClickListener) this);
            settings.setOnClickListener((View.OnClickListener) this);
            fly.setOnClickListener((View.OnClickListener) this);
            locate.setOnClickListener((View.OnClickListener) this);
            stop.setOnClickListener((View.OnClickListener) this);

            //toggles

            fpv_mode = false;
            auto_tracking_drone_mode = false;

            Switch toggleFPVMode = (Switch) findViewById(R.id.fpv_mode);
            toggleFPVMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        fpv_mode = true;
                        setResultToToast("FPV Activado");
                    } else {
                        fpv_mode = false;
                        setResultToToast("FPV Desactivado");
                    }
                    if (fpv_mode) {
                        hideConfigUI();

                    } else {
                        showConfigUI();
                    }
                }
            });

            Switch toggleAutoTrackingMode = (Switch) findViewById(R.id.auto_tracking);
            toggleAutoTrackingMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        auto_tracking_drone_mode = true;
                        setResultToToast("Auto tracking drone Activado");
                    } else {
                        auto_tracking_drone_mode = false;
                        setResultToToast("Auto tracking drone Desactivado");
                    }
                }
            });
        }

        //Video Streaming
        mVideoSurface = (TextureView)findViewById(R.id.video_previewer_surface);

    }

    private void hideConfigUI () {
        if(findViewById(R.id.routesList) != null && findViewById(R.id.satelliteAmount) !=null){
            findViewById(R.id.flyBtn).setVisibility(GONE);
            findViewById(R.id.flySettingsBtn).setVisibility(GONE);
            findViewById(R.id.loadMissionBtn).setVisibility(GONE);

            findViewById(R.id.routesList).setVisibility(GONE);;
            findViewById(R.id.satelliteAmount).setVisibility(GONE);;
            findViewById(R.id.textView4).setVisibility(GONE);
            findViewById(R.id.gpsSignalQuality).setVisibility(GONE);
            findViewById(R.id.missionCard).setVisibility(View.INVISIBLE);
            findViewById(R.id.missionCardBorder).setVisibility(View.INVISIBLE);
            findViewById(R.id.missionCardShadow).setVisibility(View.INVISIBLE);
        }
    }

    private void showConfigUI () {
        if(findViewById(R.id.routesList) != null && findViewById(R.id.satelliteAmount) !=null){
            findViewById(R.id.flyBtn).setVisibility(View.VISIBLE);
            findViewById(R.id.flySettingsBtn).setVisibility(View.VISIBLE);
            findViewById(R.id.loadMissionBtn).setVisibility(View.VISIBLE);

            findViewById(R.id.routesList).setVisibility(View.VISIBLE);;
            findViewById(R.id.satelliteAmount).setVisibility(View.VISIBLE);;
            findViewById(R.id.textView4).setVisibility(View.VISIBLE);
            findViewById(R.id.gpsSignalQuality).setVisibility(View.VISIBLE);
            findViewById(R.id.missionCard).setVisibility(View.VISIBLE);
            findViewById(R.id.missionCardBorder).setVisibility(View.VISIBLE);
            findViewById(R.id.missionCardShadow).setVisibility(View.VISIBLE);
        }
    }

    public void clearMissionMapWayPoints(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                gMapWeb.clear();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (gMapWeb == null) {
            gMapWeb = googleMap;
            gMapWeb.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
            setUpMap();
        }

        LatLng ucu = new LatLng(-34.888815, -55.159379);
        gMapWeb.addMarker(new MarkerOptions().position(ucu).title("UCU"));
        gMapWeb.moveCamera(CameraUpdateFactory.newLatLng(ucu));
        gMapWeb.moveCamera(CameraUpdateFactory.zoomIn());
    }

    private void setUpMap() {
        gMapWeb.setOnMapClickListener((GoogleMap.OnMapClickListener) this);// add the listener for click for a map object

    }

    private void markWaypoint(LatLng point){
        //Create MarkerOptions object
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        markerOptions.icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_on_list));
        Marker marker = gMapWeb.addMarker(markerOptions);
        mMarkers.put(mMarkers.size(), marker);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }



    private void markWaypointReady(LatLng point){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        Marker marker = gMapWeb.addMarker(markerOptions);
        mMarkers.put(mMarkers.size(), marker);
    }

    private void setResultToToast(final String string){
        RoutingFromWebActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(RoutingFromWebActivity.this, string, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // END CP

    public void onMapClick(LatLng point) {
        if (isAdd == true){
            markWaypoint(point);
            Waypoint mWaypoint = new Waypoint(point.latitude, point.longitude, altitude);
            //Add Waypoints to Waypoint arraylist;
            if (waypointMissionBuilder != null) {
                waypointList.add(mWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            }else
            {
                waypointMissionBuilder = new WaypointMission.Builder();
                waypointList.add(mWaypoint);
                waypointMissionBuilder.waypointList(waypointList).waypointCount(waypointList.size());
            }
        }else{
            setResultToToast("No es posible agregar Waypoints en este modo, cambiar a misión personalizada");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.locateBtn:{
                updateDroneLocation();
                cameraUpdate(); // Locate the drone's place
                break;
            }
            case R.id.flySettingsBtn:{
                showSettingDialog();
                break;
            }
            case R.id.loadMissionBtn:{
                uploadWayPointMission();
                break;
            }
            case R.id.flyBtn:{
                startWaypointMission();
                break;
            }
            case R.id.stopBtn:{
                stopWaypointMission();
                showStopDialog();
                break;
            }
            default:
                break;
        }
    }

    private void cameraUpdate(){
        LatLng pos = new LatLng(droneLocationLat, droneLocationLng);
        float zoomlevel = (float) 18.0;
        CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(pos, zoomlevel);
        gMapWeb.moveCamera(cu);

    }

    private void startWaypointMission(){
        //to clean old fly data
        isGhostWaypointReached = false;
        waypointTargetIndex = 0;
        // hideConfigUI();
        getWaypointMissionOperator().startMission(new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError error) {
                setResultToToast("La misión comienza con: " + (error == null ? "Éxito" : error.getDescription()));
            }
        });
    }

    private void stopWaypointMission(){
        this.waypointProgress += " Estado: Cancelada";
        missionStoped = true;
        getWaypointMissionOperator().stopMission(new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError error) {
                if (error == null) {
                    setResultToToast("La mision se detuvo con éxito...");
                } else {
                    setResultToToast("Error al detener la misión, error: " + error.getDescription());
                }
            }
        });
    }

    private void goHome() {
        mFlightController.startGoHome(new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    setResultToToast("La mision se detuvo, volviendo al punto de despegue...");
                } else {
                    setResultToToast("Error al detener la mision, error: " + djiError.getDescription());
                }
            }
        });
    }

    private void showStopDialog() {
        LinearLayout stopSettings = (LinearLayout) getLayoutInflater().inflate(R.layout.dialog_abort_mission, null);
        RadioGroup stopAction_RG = (RadioGroup) stopSettings.findViewById(R.id.stopActionRG);
        stopAction_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton_op_backhome){
                    goHomeSelected = true;
                } else {
                    goHomeSelected = false;
                }
            }
        });
        new AlertDialog.Builder(this)
                .setTitle("")
                .setView(stopSettings)
                .setPositiveButton("Confirmar",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {
                        if (goHomeSelected){
                            goHome();
                            setResultToToast("La mision se detuvo, volviendo al punto de despegue...");
                        } else {
                            setResultToToast("La mision se detuvo, permaneciendo en el lugar de detención...");
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();

    }

    private void showSettingDialog(){
        LinearLayout wayPointSettings = (LinearLayout)getLayoutInflater().inflate(R.layout.dialog_waypointsetting, null);

        final TextView wpAltitude_TV = (TextView) wayPointSettings.findViewById(R.id.altitude);
        final TextView wpOrientation_TV = (TextView) wayPointSettings.findViewById(R.id.orientation);
        final TextView gimbalVertical_TV = (TextView) wayPointSettings.findViewById(R.id.gimbal_vertical);


        RadioGroup speed_RG = (RadioGroup) wayPointSettings.findViewById(R.id.speed);
        RadioGroup actionAfterFinished_RG = (RadioGroup) wayPointSettings.findViewById(R.id.actionAfterFinished);
        RadioGroup heading_RG = (RadioGroup) wayPointSettings.findViewById(R.id.heading);

        speed_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.lowSpeed){
                    mSpeed = 3.0f;
                } else if (checkedId == R.id.MidSpeed){
                    mSpeed = 5.0f;
                } else if (checkedId == R.id.HighSpeed){
                    mSpeed = 10.0f;
                }
            }

        });

        actionAfterFinished_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(TAG, "Seleccionar accion al terminar");
                if (checkedId == R.id.finishNone){
                    mFinishedAction = WaypointMissionFinishedAction.NO_ACTION;
                } else if (checkedId == R.id.finishGoHome){
                    mFinishedAction = WaypointMissionFinishedAction.GO_HOME;
                } else if (checkedId == R.id.finishAutoLanding){
                    mFinishedAction = WaypointMissionFinishedAction.AUTO_LAND;
                } else if (checkedId == R.id.finishToFirst){
                    mFinishedAction = WaypointMissionFinishedAction.GO_FIRST_WAYPOINT;
                }
            }
        });

        heading_RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d(TAG, "Seleccionar heading");

                if (checkedId == R.id.headingNext) {
                    mHeadingMode = WaypointMissionHeadingMode.AUTO;
                } else if (checkedId == R.id.headingInitDirec) {
                    mHeadingMode = WaypointMissionHeadingMode.USING_INITIAL_DIRECTION;
                } else if (checkedId == R.id.headingRC) {
                    mHeadingMode = WaypointMissionHeadingMode.CONTROL_BY_REMOTE_CONTROLLER;
                } else if (checkedId == R.id.headingWP) {
                    mHeadingMode = WaypointMissionHeadingMode.USING_WAYPOINT_HEADING;
                } else if (checkedId == R.id.headingInterestPoint) {
                    mHeadingMode = WaypointMissionHeadingMode.TOWARD_POINT_OF_INTEREST;
                }
            }

        });

        //Toggle mods buttons
        Switch toggleSafeStartingMode = (Switch) wayPointSettings.findViewById(R.id.safe_starting_mode_toggle);
        toggleSafeStartingMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    safe_starting_mode = true;
                    setResultToToast("Partida Segura Activada");
                } else {
                    safe_starting_mode = false;
                    setResultToToast("Partida Segura Desactivada");
                }
            }
        });

        Switch toggleSafeReturningMode = (Switch) wayPointSettings.findViewById(R.id.safe_returning_mode_toggle);
        toggleSafeReturningMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    safe_returning_mode = true;
                    setResultToToast("Retorno Seguro Activado");


                } else {
                    safe_returning_mode = false;
                    setResultToToast("Retorno Seguro Activado");
                }
            }
        });

        Switch toggleRelativeAltitudesMode = (Switch) wayPointSettings.findViewById(R.id.relative_heights_mode_toggle);
        toggleRelativeAltitudesMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    relative_altitudes_mode = true;
                    setResultToToast("Alturas Relativas Activadas");
                } else {
                    relative_altitudes_mode = false;
                    setResultToToast("Alturas Relativas Desactivadas");
                }
            }
        });

        new AlertDialog.Builder(this)
                .setTitle("")
                .setView(wayPointSettings)
                .setPositiveButton("Confirmar",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id) {
                        String altitudeString = wpAltitude_TV.getText().toString();
                        altitude = Integer.parseInt(nulltoIntegerDefault(altitudeString));
                        String orientationString = wpOrientation_TV.getText().toString();
                        waypointHeading = Integer.parseInt(nulltoIntegerDefault(orientationString));
                        String gimbalVerticalString = gimbalVertical_TV.getText().toString();
                        gimbalVertical = Integer.parseInt(nulltoIntegerDefault(gimbalVerticalString));
                        Log.e(TAG,"altitude "+altitude);
                        Log.e(TAG,"heading "+altitude);
                        Log.e(TAG,"gimbal vertical  "+altitude);
                        Log.e(TAG,"speed "+mSpeed);
                        Log.e(TAG, "mFinishedAction "+mFinishedAction);
                        Log.e(TAG, "mHeadingMode "+mHeadingMode);
                        configWayPointMission();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }





    private void configWayPointMission(){

        if (waypointMissionBuilder == null){

            waypointMissionBuilder = new WaypointMission.Builder().finishedAction(mFinishedAction)
                    .headingMode(mHeadingMode)
                    .autoFlightSpeed(mSpeed)
                    .maxFlightSpeed(mSpeed)
                    .flightPathMode(WaypointMissionFlightPathMode.NORMAL)
                    .setPointOfInterest(Default_InterestPoint)
                    .setExitMissionOnRCSignalLostEnabled(true)
                    .setGimbalPitchRotationEnabled(true);
        }else
        {
            waypointMissionBuilder.finishedAction(mFinishedAction)
                    .headingMode(mHeadingMode)
                    .autoFlightSpeed(mSpeed)
                    .maxFlightSpeed(mSpeed)
                    .flightPathMode(WaypointMissionFlightPathMode.NORMAL)
                    .setPointOfInterest(Default_InterestPoint)
                    .setExitMissionOnRCSignalLostEnabled(true)
                    .setGimbalPitchRotationEnabled(true);
        }

        if (waypointMissionBuilder.getWaypointList().size() > 0){
            //CREATING ACTION
            int actionParam = 0;
            WaypointActionType waypointActionType = WaypointActionType.START_TAKE_PHOTO;
            WaypointAction waypointAction = new WaypointAction(waypointActionType, actionParam);

            // trigger for progress
            WaypointReachedTrigger trigger = new WaypointReachedTrigger();

            //Adding waypoint Heading to the ghost point
            waypointMissionBuilder.getWaypointList().get(0).setHeadingInner(waypointHeading);
            float gimbal = (float) gimbalVertical;
            waypointMissionBuilder.getWaypointList().get(0).gimbalPitch = gimbal;

            //Start from 1 because ghost waypoint doenst need this setup
            for (int i=1; i< waypointMissionBuilder.getWaypointList().size(); i++){
                // waypoint height actually is the waypoint's place height over the see level,
                // altitude the take off location height over the see level, and must be setted by the user using the  contour lines map
                // and we plus it 2 meters (the height we expect in the photos)
                if (relative_altitudes_mode) {
                    waypointMissionBuilder.getWaypointList().get(i).altitude -= altitude - 2;
                } else {
                    waypointMissionBuilder.getWaypointList().get(i).altitude = altitude;
                }

            //Especial Altitude from Height to Low
            // offset is the estimated height offset between every tree
            // float offset = 0.313f;
            //waypointMissionBuilder.getWaypointList().get(i).altitude = altitude - (i * offset) ;

            //ADDING ACTION TO WAYPOINT
            waypointMissionBuilder.getWaypointList().get(i).addAction(waypointAction);

            //Adding waypoint amount to trigger
            trigger.setWaypointIndex(i);

            //Adding waypoint orientation
            waypointMissionBuilder.getWaypointList().get(i).setHeadingInner(waypointHeading);

            //Adding waypoint gimbal vertical
            waypointMissionBuilder.getWaypointList().get(i).gimbalPitch = gimbal;

        }
    }

        //SAFE RETURNING MODE TOGGLE
        int goHomeMeters;
        if (safe_returning_mode) {
            goHomeMeters = 30;
        } else {
            goHomeMeters = 20;
        }

        mFlightController.setGoHomeHeightInMeters(goHomeMeters, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError == null) {
                    setResultToToast("Go Home a " + goHomeMeters + " metros");
                } else {
                    setResultToToast("No fue posible configuar la altura de Go Home, error: " + djiError.getDescription());
                }
            }
        });

        //SAFE STARTING MODE TOGGLE
        if(!safe_starting_mode){
            if (waypointMissionBuilder != null) {
                if (waypointMissionBuilder.getWaypointCount() > 0) {
                    waypointMissionBuilder.removeWaypoint(0);
                }
            }
        }

        DJIError error = getWaypointMissionOperator().loadMission(waypointMissionBuilder.build());
        if (error == null) {
            setResultToToast("Waypoints cargados con éxito");
        } else {
            setResultToToast("La carga de waypoints falló: " + error.getDescription());
        }
    }

    public void clearWaypointMission () {
        if (waypointMissionBuilder != null) {
            if (waypointMissionBuilder.getWaypointCount() > 0) {
                int waypointsAmount = waypointMissionBuilder.getWaypointCount();
                for (int i = 0; i < waypointsAmount; i++) {
                    waypointMissionBuilder.removeWaypoint(i);
                }
                //setResultToToast("Hay waypoints number: " + Integer.toString(waypointMissionBuilder.getWaypointList().size()));
            } else {
                //setResultToToast("esta vacia");
            }
        }

        isGhostWaypointReached = false;
        waypointTargetIndex = 0;
        waypointReady = 0;
    }

    String nulltoIntegerDefault(String value){
        if(!isIntValue(value)) value="0";
        return value;
    }

    boolean isIntValue(String val)
    {
        try {
            val=val.replace(" ","");
            Integer.parseInt(val);
        } catch (Exception e) {return false;}
        return true;
    }

    private void uploadWayPointMission(){
        missionComplete = false;
        missionStoped = false;
        waypointTargetIndex = 0;
        waypointReady = 0;
        mMarkers.get(waypointAmount-1).setIcon((bitmapDescriptorFromVector(getApplicationContext(), R.drawable.ic_tree_on_list)));

        getWaypointMissionOperator().uploadMission(new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError error) {
                if (error == null) {
                    setResultToToast("Mission cargada con éxito!");
                } else {
                    setResultToToast("Error al subir la misión, error: " + error.getDescription() + " reintentando...");
                    getWaypointMissionOperator().retryUploadMission(null);
                }
            }
        });

    }

    //Implement interface for video streaming

    private void uninitPreviewer() {
        Camera camera = DroneApplication.getCameraInstance();
        if (camera != null){
            // Reset the callback
            VideoFeeder.getInstance().getPrimaryVideoFeed().addVideoDataListener(null);
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureAvailable");
        if (mCodecManager == null) {
            mCodecManager = new DJICodecManager(this, surface, width, height);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        Log.e(TAG,"onSurfaceTextureDestroyed");
        if (mCodecManager != null) {
            mCodecManager.cleanSurface();
            mCodecManager = null;
        }

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }


    private void initPreviewer() {

        BaseProduct product = DroneApplication.getProductInstance();

        if (product == null || !product.isConnected()) {
            showToast(getString(R.string.disconnected));
        } else {
            if (null != mVideoSurface) {
                mVideoSurface.setSurfaceTextureListener(this);
            }
            if (!product.getModel().equals(Model.UNKNOWN_AIRCRAFT)) {
                VideoFeeder.getInstance().getPrimaryVideoFeed().addVideoDataListener(mReceivedVideoDataListener);
            }
        }
    }

    public void showToast(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(RoutingFromWebActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
