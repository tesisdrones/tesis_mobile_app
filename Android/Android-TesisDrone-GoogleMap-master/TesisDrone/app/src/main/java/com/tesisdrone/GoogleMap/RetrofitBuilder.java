package com.tesisdrone.GoogleMap;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitBuilder {
    private static RetrofitBuilder instance = null;
    private Api myApi;

    private RetrofitBuilder() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myApi = retrofit.create(Api.class);
    }

    public static synchronized RetrofitBuilder getInstance() {
        if (instance == null) {
            instance = new RetrofitBuilder();
        }
        return instance;
    }

    public Api getMyApi() {
        return myApi;
    }
}
