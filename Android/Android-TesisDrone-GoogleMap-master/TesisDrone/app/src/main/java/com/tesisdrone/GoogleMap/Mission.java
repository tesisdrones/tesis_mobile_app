package com.tesisdrone.GoogleMap;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class Mission {
    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("routeData")
    public List<RouteData> routeData = null;

    public Mission(String id, String name,List<RouteData> routeData) {
        this.name = name;
        this.id = id;
        this.routeData = routeData;
    }

    public String getId() { return id; }
    public void setId(String id) { this.id = id; }


    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    // WAYPOINTS

    public List<List<Double>> getWaypointCoordinatesList() {
        List<List<Double>> waypointList = new LinkedList<List<Double>>();
        for (int i = 0; i < routeData.size(); i++) {
            waypointList.add(routeData.get(i).getWaypointCoordinatesList());
        }
        return waypointList;
    }

    public Double[][] getWaypointCoordinatesArray() {
        List<List<Double>> waypointCoordinatesList = this.getWaypointCoordinatesList();
        Double[][] waypointCoordinatesArray = new Double [waypointCoordinatesList.size()][2];
        for (int i=0; i<waypointCoordinatesArray.length; i++){
            waypointCoordinatesArray[i][0] =  waypointCoordinatesList.get(i).get(0);
            waypointCoordinatesArray[i][1] =  waypointCoordinatesList.get(i).get(1);
        }
        return waypointCoordinatesArray;
    }

    // TREE

    public List<List<Double>> getTreeCoordinatesList() {
        List<List<Double>> treeList = new LinkedList<List<Double>>();
        for (int i = 0; i < routeData.size(); i++) {
            treeList.add(routeData.get(i).getTreeCoordinatesList());
        }
        return treeList;
    }

    public Double[][] getTreeCoordinatesArray() {
        List<List<Double>> treeCoordinatesList = this.getTreeCoordinatesList();
        Double[][] treeCoodinatesArray = new Double [treeCoordinatesList.size()][2];
        for (int i=0; i<treeCoodinatesArray.length; i++){
            treeCoodinatesArray[i][0] =  treeCoordinatesList.get(i).get(0);
            treeCoodinatesArray[i][1] =  treeCoordinatesList.get(i).get(1);
        }
        return treeCoodinatesArray;
    }

    // HEIGHT

    public List<Double> getHeightList() {
        List<Double> heightList = new LinkedList<Double>();
        for (int i = 0; i < routeData.size(); i++) {
            heightList.add(routeData.get(i).getHeight());
        }
        return heightList;
    }

    public Double[] getHeightArray() {
        List<Double> heightList = this.getHeightList();
        Double[] treeCoordinatesArray = new Double [heightList.size()];
        for (int i=0; i<treeCoordinatesArray.length; i++){
            treeCoordinatesArray[i] =  heightList.get(i);
        }
        return treeCoordinatesArray;
    }
}
